package br.com.itau;

public class Carta {

    private char carta;
    private char naipe;
    private int valor;

    public Carta(int carta, char naipe) {
        switch(carta){
            case 1:
                this.carta = 'A'; valor = carta; break;
            case 10:
                this.carta = 'T'; valor = 10; break;
            case 11:
                this.carta = 'J'; valor = 10; break;
            case 12:
                this.carta = 'Q'; valor = 10;break;
            case 13:
                this.carta = 'K'; valor = 10;break;
            default:
                this.carta = Character.forDigit(carta, 10); valor = carta;
        }

        this.naipe = naipe;

    }

    public char getCarta() {
        return carta;
    }

    public char getNaipe() {
        return naipe;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }
}
