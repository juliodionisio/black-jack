package br.com.itau;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int record = 0;
        int ultimaPontuacao = 0;
        Scanner input = new Scanner(System.in);
        Jogo jogo = new Jogo();
        boolean jogando = true;
        while(jogando){
            System.out.println("Digite s para iniciar um novo jogo.");
            if(input.next().equals("s")){
                ultimaPontuacao = jogo.iniciarJogo();
                if(ultimaPontuacao > record) record = ultimaPontuacao;
                System.out.println("O record é " + record);
            }
        }


    }
}
