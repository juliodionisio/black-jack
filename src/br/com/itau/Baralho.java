package br.com.itau;


import java.util.LinkedList;
import java.util.Random;

public class Baralho {


    private LinkedList<Carta> baralho = new LinkedList<Carta>();

    public LinkedList<Carta> getBaralho() {
        return baralho;
    }

    public void setBaralho(LinkedList<Carta> baralho) {
        this.baralho = baralho;
    }

    public void baralhoCompleto() {
        for(int valorCarta = 1; valorCarta<14; valorCarta++){

            //System.out.println("Carta: " + valorCarta);
            baralho.add(new Carta(valorCarta, 'S'));
            baralho.add(new Carta(valorCarta, 'C'));
            baralho.add(new Carta(valorCarta, 'H'));
            baralho.add(new Carta(valorCarta, 'D'));
        }

    }

    public void validarBaralho(){
        System.out.println("Cartas na mão do jogador: ");

        for(int n = 0; n < baralho.size(); n++) {

            System.out.println("Carta: " + baralho.get(n).getCarta() + baralho.get(n).getNaipe());
        }
    }

    public Carta distribuirCarta(){

        Carta carta = baralho.get((int)(Math.random() * (baralho.size())));
        baralho.remove(carta);
        return carta;

    }

    public int somarMao(){
        int somaTotal = 0;
        for(int n = 0; n < baralho.size(); n++) {
            somaTotal = baralho.get(n).getValor() + somaTotal;
        }
        return somaTotal;
    }



}
