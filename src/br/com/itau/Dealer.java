package br.com.itau;

public class Dealer {

    Baralho baralho = new Baralho();


    public void iniciarBaralho(){
        baralho.baralhoCompleto();
        baralho.validarBaralho();
    }

    public void distribuirCartas(Jogador jogador){
        jogador.getMao().getBaralho().add(baralho.distribuirCarta());
        jogador.getMao().validarBaralho();
    }






}
