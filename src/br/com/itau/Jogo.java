package br.com.itau;

import java.util.Scanner;

public class Jogo {



    public int iniciarJogo(){
        Dealer dealer = new Dealer();
        Jogador jogador = new Jogador();
        Scanner input = new Scanner(System.in);
        int record = 0;

        System.out.println("Listando cartas do baralho");
        dealer.iniciarBaralho();
        System.out.println("Distribuindo cartas iniciais");
        dealer.distribuirCartas(jogador);
        dealer.distribuirCartas(jogador);

        System.out.println("Valor total da mão: " + jogador.getMao().somarMao());

        boolean maisCartas = true;

        while(maisCartas) {
            System.out.println("Digite s para receber uma nova carta ou qualquer outro caractere para encerrar.");
            if(input.next().equals("s")){
                dealer.distribuirCartas(jogador);
                if (jogador.getMao().somarMao() > 21) {
                    System.out.println("Você estourou 21 pontos!");
                    maisCartas = false;
                    return 0;
                } if (jogador.getMao().somarMao() == 21){
                    System.out.println("Parabéns! Você conseguiu 21 pontos");

                }
            }else {
                System.out.println("Você conseguiu "+jogador.getMao().somarMao()+ " pontos.");
                return jogador.getMao().somarMao();
                //maisCartas = false;
            }

        }
        return jogador.getMao().somarMao();

    }


}

